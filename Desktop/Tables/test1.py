import cv2
import numpy as np

# Load the image
image = cv2.imread("C://Users//Frki//Desktop//Tables//car.jpg")
image = cv2.resize(image, (800, 600))

# Define the path to the pre-trained EAST text detection model
model_path = "frozen_east_text_detection.pb"

# Load the pre-trained EAST text detection model
net = cv2.dnn.readNet(model_path)

# Define the output layer names for the EAST model
layer_names = [
    "feature_fusion/Conv_7/Sigmoid",
    "feature_fusion/concat_3"
]

# Define the minimum confidence score for the detected text regions
min_confidence = 0.5

# Extract the dimensions of the image
(h, w) = image.shape[:2]

# Prepare the image for the EAST model
blob = cv2.dnn.blobFromImage(
    image, 1.0, (w, h),
    (123.68, 116.78, 103.94),
    swapRB=True, crop=False
)

# Pass the blob through the EAST model and obtain the scores and geometry
net.setInput(blob)
(scores, geometry) = net.forward(layer_names)

# Decode the predictions and apply non-maxima suppression
rects, confidences = cv2.dnn.NMSBoxesRotated(
    boxes=geometry[0, :, :],
    scores=scores[0],
    score_threshold=min_confidence,
    nms_threshold=0.4,
    min_size=(5, 5)
)

# Loop over the bounding boxes and draw them on the image
for i in range(len(rects)):
    # Extract the coordinates and angle of the bounding box
    (cx, cy), (w, h), angle = rects[i]
    # Extract the sine and cosine of the angle
    sin = np.abs(np.sin(angle * np.pi / 180.0))
    cos = np.abs(np.cos(angle * np.pi / 180.0))

    # Compute the dimensions of the bounding box after the rotation
    new_w = int(w * cos + h * sin)
    new_h = int(w * sin + h * cos)

    # Compute the top-left and bottom-right coordinates of the bounding box
    x = int(cx - new_w / 2)
    y = int(cy - new_h / 2)
    x2 = int(cx + new_w / 2)
    y2 = int(cy + new_h / 2)

    # Extract the license plate from the image
    plate = image[y:y2, x:x2]

    # Display the license plate
    cv2.imshow("License Plate", plate)
    cv2.waitKey(0)
