import cv2
import numpy as np
from matplotlib import pyplot as plt
import imutils
import pytesseract
filename = "C://Users//Frki//Desktop//Tables//archive//images//pezo.png"
car_img = cv2.imread(filename)
car_img = cv2.cvtColor(car_img, cv2.COLOR_BGR2RGB)


h, w, _ = car_img.shape
h_center, w_center = h // 2, w // 2
crop_size_h, crop_size_w = h // 4, w // 4
car_img = car_img[h_center-crop_size_h:h_center+crop_size_h, w_center-crop_size_w:w_center+crop_size_w]


gray = cv2.cvtColor(car_img, cv2.COLOR_RGB2GRAY)
blurred_gray = cv2.bilateralFilter(gray, 60, 0, 200)
edged = cv2.Canny(blurred_gray, 50, 150)

contours = cv2.findContours(edged.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
contours = imutils.grab_contours(contours)
contours = sorted(contours, key=cv2.contourArea, reverse=True)[:10]
screenCnt = None

for c in contours:
    peri = cv2.arcLength(c, True)
    approx = cv2.approxPolyDP(c, 0.018 * peri, True)
    if len(approx) == 4:
        screenCnt = approx
        break

if screenCnt is None:
    detected = 0
    print("No contour detected")
    plt.imshow(cv2.cvtColor(car_img, cv2.COLOR_RGB2BGR))
    plt.show()
else:
    detected = 1
    cv2.drawContours(car_img, [screenCnt], -1, (0, 0, 255), 3)

    mask = np.zeros(gray.shape, np.uint8)
    new_image = cv2.drawContours(mask, [screenCnt], 0, 255, -1,)
    new_image = cv2.bitwise_and(car_img, car_img, mask=mask)
    cv2.imwrite("C://Users//Frki//Desktop//Tables//new_image.jpg", new_image)
    plt.imshow(cv2.cvtColor(car_img, cv2.COLOR_RGB2BGR))
    plt.show()
    



# Set the path to the tesseract executable
pytesseract.pytesseract.tesseract_cmd = r"C:/Program Files/Tesseract-OCR/tesseract.exe"

# Read the new_image using OpenCV
plate_img = cv2.imread("C://Users//Frki//Desktop//Tables//new_image.jpg")
gray_plate = cv2.cvtColor(plate_img, cv2.COLOR_BGR2GRAY)

# Increase the contrast of the image
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
gray_plate = clahe.apply(gray_plate)

# Perform adaptive thresholding on the grayscale image
thresh = cv2.adaptiveThreshold(
    gray_plate, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)

# Apply image dilation to make the text more readable
kernel = np.ones((3, 3), np.uint8)
dilation = cv2.dilate(thresh, kernel, iterations=1)

# Use PyTesseract to extract text from the image
text = pytesseract.image_to_string(dilation, config='--psm 6 -c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')

print(text)

# Show the thresholded image
cv2.imshow('Thresholded Image', dilation)
cv2.waitKey(0)
cv2.destroyAllWindows()
